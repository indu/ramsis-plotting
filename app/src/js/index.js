import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/ramsis/App';

if (document.getElementById('react-ramsis'))
    ReactDOM.render(<App />, document.getElementById('react-ramsis'));
