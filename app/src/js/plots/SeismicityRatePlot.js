import React from 'react';
import Plot from 'react-plotly.js';
import moment from 'moment';
import { median, quantile, round } from '../utils/helpers';

export default function SeismicityRatePlot({ results, hydraulics, injectionplan, seismicity }) {
    // Result Samples
    let { samples } = results.result.subgeometries[0];
    samples = samples.sort((a, b) => moment(a.starttime) - moment(b.starttime));

    // Fitted Data
    let fitSamples = samples.filter((sample) => sample.numberevents.value);

    // let fitDates = fitSamples.map((sample) =>
    //    moment(sample.starttime)
    //        .add(moment(sample.endtime).diff(sample.starttime, 'ms') / 2, 'ms')
    //        .valueOf()
    // );
    let fitDates = fitSamples.map((sample) => moment(sample.endtime).valueOf());

    // Predicted data
    let predSamples = samples.filter((sample) => sample.numberevents.pdfvariable);

    // let predDates = predSamples.map((sample) =>
    //    moment(sample.starttime)
    //        .add(moment(sample.endtime).diff(sample.starttime, 'ms') / 2, 'ms')
    //        .valueOf()
    // );
    let predDates = predSamples.map((sample) => moment(sample.endtime).valueOf());

    let y = predSamples.map((s) => median(s.numberevents.pdfvariable));
    let mc = predSamples.map((s) => s.mc.value);
    let bValue = predSamples.map((s) => median(s.b.pdfvariable));
    let yLower = predSamples.map((s) => quantile(s.numberevents.pdfvariable, 0.25));
    let yUpper = predSamples.map((s) => quantile(s.numberevents.pdfvariable, 0.75));

    // Observed hydraulics
    hydraulics = hydraulics.samples;
    hydraulics = hydraulics.sort((a, b) => moment(a.datetime.value) - moment(b.datetime.value));
    let flowDates = hydraulics.map((hydSample) => moment(hydSample.datetime.value).valueOf());
    let startPred = predDates.sort((a, b) => a - b)[0];
    flowDates = flowDates.filter((fd) => fd < startPred);
    let flow = hydraulics.map((hydSample) => (hydSample.topflow?.value || 0) * 60000);

    // Injectionplan
    injectionplan = injectionplan.samples;
    injectionplan = injectionplan.sort(
        (a, b) => moment(a.datetime.value) - moment(b.datetime.value)
    );
    let planDates = injectionplan
        .map((hydSample) => moment(hydSample.datetime.value).valueOf())
        .filter((fd) => fd >= startPred);
    let plan = injectionplan.map((hydSample) => (hydSample.topflow?.value || 0) * 60000);

    // This should actually be loaded from the database
    let alpha = predSamples.map((s) => median(s.alpha?.pdfvariable || [0]));

    // connect observed to planned hydraulics
    plan.unshift(flow[flowDates.length - 1]);
    planDates.unshift(flowDates[flowDates.length - 1]);

    // observed seismicity
    seismicity.events = seismicity.events.sort(
        (a, b) => moment(a.datetime.value) - moment(b.datetime.value)
    );
    console.log(seismicity);
    let EVAboveCompletion = seismicity.events.filter((ev) => ev.magnitude.value > mc.slice(-1));
    let magDates = EVAboveCompletion.map((ev) => moment(ev.datetime.value).valueOf());

    let labelModel = `Model Fit (Mc: ${mc.slice(-1).toString()}, b: ${round(
        bValue.slice(-1),
        3
    ).toString()}, af: ${round(alpha.slice(-1), 3).toString()})`;

    let dataRate = [];

    for (let i = 0; i < fitDates.length; i++) {
        if (i === 0) {
            dataRate[i] = magDates.filter((fd) => fd < fitDates[i]).length;
        } else {
            dataRate[i] = magDates.filter((fd) => fd > fitDates[i - 1] && fd < fitDates[i]).length;
        }
    }

    let recData = {
        x: fitDates,
        y: dataRate,
        mode: 'lines',
        type: 'scatter',
        name: 'Data',
        line: { width: 2, color: 'gray' },
    };

    let fitRate = {
        x: fitDates,
        y: fitSamples.map((s) => s.numberevents.value),
        mode: 'lines',
        type: 'scatter',
        name: labelModel,
        line: { width: 3, color: 'red' },
    };

    let predRate = {
        x: predDates,
        y,
        line: { color: 'rgb(0,100,80)', shape: 'hv' },
        mode: 'lines',
        name: 'Predicted Rate',
        type: 'scatter',
    };

    let predDates2 = predSamples.map((sample) =>
        moment(sample.endtime)
            .add(moment(sample.endtime).diff(sample.starttime, 'ms') / 2, 'ms')
            .valueOf()
    );
    let predRateBound = {
        x: predDates2.concat(predDates2.slice().reverse()),
        y: yUpper.concat(yLower.slice().reverse()),
        fill: 'toself',
        fillcolor: 'rgba(0,100,80,0.2)',
        line: { color: 'transparent', shape: 'hvh' },
        name: 'Predicted Rate bounds',
        type: 'scatter',
    };

    let flowRate = {
        x: flowDates,
        y: flow,
        type: 'scatter',
        mode: 'lines',
        name: 'Interval Flow',
        line: { color: 'blue' },
        yaxis: 'y2',
        opacity: 0.1,
    };

    let flowRatePlan = {
        x: planDates,
        y: plan,
        type: 'scatter',
        mode: 'lines',
        name: 'Injection Plan Scenario',
        line: { color: 'darkblue', shape: 'vh' },
        yaxis: 'y2',
        opacity: 0.5,
    };

    let dataplot = [recData, fitRate, predRate, predRateBound, flowRate, flowRatePlan];
    let layout = {
        title: 'Seismicity rate forecast',
        autosize: true,
        responsive: true,
        xaxis: {
            type: 'date',
            title: 'Time [UTC]',
            domain: [0, 0.9],
        },
        yaxis: {
            title: 'Rate of Events',
            titlefont: { color: 'rgb(0,100,80)' },
            tickfont: { color: 'rgb(0,100,80)' },
        },
        yaxis2: {
            title: 'Flowrate [l/min]',
            overlaying: 'y',
            side: 'right',
            titlefont: { color: 'blue' },
            tickfont: { color: 'blue' },
        },
    };

    return <Plot data={dataplot} layout={layout} config={{ responsive: true }} />;
}
