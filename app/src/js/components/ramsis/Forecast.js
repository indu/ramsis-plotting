import React from 'react';
import moment from 'moment';
import { getForecastsByProject } from '../../utils/api';

function CurrentForecast({ forecast }) {
    return (
        <div className="panel-body">
            {forecast ? (
                <div className="container-fluid">
                    <div className="page-header">
                        <h3 className="h2">
                            {forecast.name} <small>Status: {forecast.status}</small>
                        </h3>
                    </div>
                    <div className="row rs-properties">
                        <ul className="list-group col-xs-12 col-md-6 rs-properties__list">
                            <li className="list-group-item rs-properties__item">
                                <span>Starttime</span>
                                <span>{forecast.starttime}</span>
                            </li>
                            <li className="list-group-item rs-properties__item">
                                <span>Endtime</span>
                                <span>{forecast.endtime}</span>
                            </li>
                            <li className="list-group-item rs-properties__item">
                                <span>Creationtime</span>
                                <span>{forecast.creationinfo.creationtime}</span>
                            </li>
                        </ul>
                        <ul className="list-group col-xs-12 col-md-6 rs-properties__list">
                            <li className="list-group-item rs-properties__item">
                                <span>Catalog</span>
                                <a
                                    href={`
                                      ${process.env.API_URL}/v1/forecast/${forecast.id}/catalog`}
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    link
                                </a>
                            </li>
                            <li className="list-group-item rs-properties__item">
                                <span>Hydraulics</span>
                                <a
                                    href={`${process.env.API_URL}/v1/forecast/${forecast.id}/well`}
                                    target="_blank"
                                    rel="noreferrer"
                                >
                                    link
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            ) : (
                <span>Please Select a Forecast</span>
            )}
        </div>
    );
}

function Forecast({ projectID, selectedForecast, setForecast }) {
    const [forecasts, setForecasts] = React.useState([]);
    const [status, setStatus] = React.useState('loading');

    React.useEffect(() => {
        if (projectID) {
            getForecastsByProject(projectID)
                .then((json) =>
                    setForecasts(json.sort((a, b) => moment(a.starttime) - moment(b.starttime)))
                )
                .catch((e) => {
                    console.error(`Error ${e} ocurred fetching Forecasts.`);
                    setStatus('error');
                    setForecasts([]);
                    setForecast(null);
                });
        }

        return setForecast(null);
    }, [projectID]);

    return (
        <div className="panel panel-success rs-box">
            <div className="panel-heading">
                <div className="rs-box__heading">
                    <h2 className="h2">Forecasts</h2>
                </div>
            </div>
            <CurrentForecast forecast={selectedForecast} />

            {projectID ? (
                <ul className="list-group rs-list rs-list--fixed">
                    {forecasts ? (
                        forecasts.map((forecast) => (
                            <li
                                className={`list-group-item rs-list__item ${
                                    forecast.id === selectedForecast?.id
                                        ? 'list-group-item-warning'
                                        : ''
                                }`}
                                key={`forecast_${forecast.id}`}
                            >
                                <div className="container-fluid">
                                    <div className="row">
                                        <div className="col-xs-3">{forecast.name}</div>
                                        <div className="col-xs-3">{forecast.starttime}</div>
                                        <div className="col-xs-3">{forecast.endtime}</div>
                                        <div className="col-xs-2">{forecast.status}</div>
                                        <div className="col-xs-1">
                                            <button
                                                className="btn btn-xs btn-success"
                                                type="button"
                                                onClick={() => setForecast(forecast)}
                                            >
                                                select
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        ))
                    ) : (
                        <div>{status} </div>
                    )}
                </ul>
            ) : (
                <div className="panel-body">No Project Selected</div>
            )}
        </div>
    );
}
export default Forecast;
