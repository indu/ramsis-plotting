import React from 'react';
import Plot from 'react-plotly.js';
// import moment from 'moment';

export default function SeismicityPlot({ data, startDate, endDate }) {
    // console.log(data.filter((x) => !('time' in x.origin)));

    // data = data.map((e) => {
    //     if (!('time' in e.origin)) {
    //         e.origin = e.origin[0];
    //     }
    //     if (!('magnitude' in e)) {
    //         e.magnitude = { mag: { value: 0 } };
    //     }
    //     return e;
    // });
    // data = data.filter((x) => 'time' in x.origin && 'value' in x.origin.time);

    data.sort((a, b) => b.origin.time.value - a.origin.time.value);

    let x = data.map((e) => e.origin.time.value);
    let mag = data.map((e) => e.magnitude.mag.value);

    // let x = data.dataset.cehour.split(',').map(Number);
    // let y = data.dataset.ceeventcount.split(',').map(Number);
    // let name = data.dataset.ceeventname.split(',');
    // let mag = data.dataset.cemag.split(',').map(Number);

    return (
        <Plot
            data={[
                {
                    x,
                    y: mag,
                    mode: 'markers',
                    type: 'scatter',
                    name: 'Magnitudes',
                    yaxis: 'y2',
                },
                // {
                //     x,
                //     y,
                //     mode: 'lines',
                //     type: 'scatter',
                //     name: 'Cumulative Events',
                // },
            ]}
            layout={{
                title: 'Seismics',
                autosize: true,
                responsive: true,
                xaxis: {
                    range: [startDate, endDate],
                    title: 'datetime',
                    type: 'date',
                },
                yaxis: {
                    title: 'Cumulative Number of Earthquakes',
                },
                yaxis2: {
                    title: 'Magnitude',
                    overlaying: 'y',
                    side: 'right',
                },
            }}
            config={{ responsive: true }}
        />
    );
}
