import React from 'react';
import { getWell, getForecastSeismicity } from '../../utils/api';
import SeismicityStage from './SeismicityStage';

function Scenario({ forecast }) {
    const [seismicity, setSeismicity] = React.useState(null);
    const [hydraulics, setHydraulics] = React.useState(null);

    React.useEffect(() => {
        if (forecast) {
            getForecastSeismicity(forecast.id)
                .then((json) => setSeismicity(json))
                .catch((e) => {
                    console.warn(`Error occurred fetching seismic catalog: ${e}`);
                    setSeismicity('error');
                });
            getWell(forecast.id)
                .then((json) => setHydraulics(json))
                .catch((e) => {
                    console.warn(`Error occurred fetching hydraulics: ${e}`);
                    setHydraulics('error');
                });
        }
        return () => {
            setHydraulics(null);
            setSeismicity(null);
        };
    }, [forecast]);

    return (
        <>
            {forecast ? (
                forecast.scenarios.map((id) => (
                    <SeismicityStage
                        key={`scenariorun_${id}`}
                        hydraulics={hydraulics}
                        seismicity={seismicity}
                        scenarioId={id}
                    />
                ))
            ) : (
                <div className="panel panel-info rs-box">
                    <div className="panel-heading">
                        <div className="rs-box__heading">
                            <h2 className="h2">Scenarios</h2>
                        </div>
                    </div>
                    <div className="panel-body">Please select a Forecast.</div>
                </div>
            )}
        </>
    );
}

export default Scenario;
