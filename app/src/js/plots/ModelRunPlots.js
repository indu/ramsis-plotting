import React from 'react';
import MagnitudeFreuencyPlot from './MagnitudeFrequencyPlot';
import SeismicityRatePlot from './SeismicityRatePlot';

function ModelRunPlots({ results, hydraulics, injectionplan, seismicity }) {
    return (
        <>
            <SeismicityRatePlot
                results={results}
                hydraulics={hydraulics}
                injectionplan={injectionplan}
                seismicity={seismicity}
            />
            <MagnitudeFreuencyPlot results={results} seismicity={seismicity} />
        </>
    );
}
export default ModelRunPlots;
