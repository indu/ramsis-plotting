import React from 'react';
import Forecast from './Forecast';
import Projects from './Projects';
import Scenario from './Scenario';

function App() {
    const [project, setProject] = React.useState(null);
    const [forecast, setForecast] = React.useState(null);

    return (
        <>
            <Projects selectedProject={project} setProject={setProject} />
            <Forecast
                projectID={project?.id}
                selectedForecast={forecast}
                setForecast={setForecast}
            />
            <Scenario forecast={forecast} />
        </>
    );
}

export default App;
