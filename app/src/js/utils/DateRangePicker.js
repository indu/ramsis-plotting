import React from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.min.css';

export default function DateRangePicker({ start, setStart, end, setEnd, onSubmit, disabled }) {
    return (
        <div className="form-inline">
            <div className="form-group">
                <label htmlFor="datepicker-start">Startdate:</label>
                <DatePicker
                    id="datepicker-start"
                    selected={start}
                    dateFormat="dd/MM/yyyy HH:mm"
                    timeFormat="HH:mm"
                    showTimeSelect
                    onChange={(date) => setStart(date)}
                    selectsStart
                    className="form-control"
                    startDate={start}
                    endDate={end}
                />
            </div>
            <div className="form-group">
                <label htmlFor="datepicker-start">Enddate:</label>
                <DatePicker
                    id="datepicker-end"
                    selected={end}
                    dateFormat="dd/MM/yyyy HH:mm"
                    timeFormat="HH:mm"
                    showTimeSelect
                    onChange={(date) => setEnd(date)}
                    selectsEnd
                    className="form-control"
                    startDate={start}
                    endDate={end}
                    minDate={start}
                    maxDate={new Date()}
                />
            </div>

            <button
                type="submit"
                className="btn btn-primary form-control"
                style={{ verticalAlign: 'bottom' }}
                onClick={onSubmit}
                disabled={disabled}
            >
                Select
            </button>
        </div>
    );
}
