import React from 'react';
import moment from 'moment';
import { getProjects } from '../../utils/api';

function ProjectsBox(props) {
    return (
        <div className="panel panel-primary rs-box">
            <div className="panel-heading">
                <div className="rs-box__heading">
                    <h2 className="h1">Project</h2>
                    <button
                        className="btn btn-warning btn-lg"
                        type="button"
                        data-toggle="collapse"
                        href={props.collapseID}
                    >
                        Select Project &nbsp;&nbsp;
                        <span className="glyphicon glyphicon-chevron-down" aria-hidden="true" />
                    </button>
                </div>
            </div>
            {props.children}
        </div>
    );
}

function CurrentProject({ project }) {
    return (
        <div className="panel-body">
            <div className="container-fluid">
                <div className="page-header">
                    <h3 className="h2">
                        {project.name} <small>{project.description}</small>
                    </h3>
                </div>
                <div className="row rs-properties">
                    <ul className="list-group col-xs-12 col-md-6 rs-properties__list">
                        <li className="list-group-item rs-properties__item">
                            <span>Starttime</span>
                            <span>{project.starttime}</span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>Endtime</span>
                            <span>{project.endtime}</span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>Creationtime</span>
                            <span>{project.creationinfo.creationtime}</span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>PROJ String</span>
                            <span>{project.proj_string}</span>
                        </li>
                    </ul>
                    <ul className="list-group col-xs-12 col-md-6">
                        <li className="list-group-item list-group-item-info">Settings</li>
                        {Object.keys(project.settings.config).map((key) => (
                            <li
                                key={`projectsetting_${key}`}
                                className="list-group-item rs-properties__item"
                            >
                                <span>{key}</span>
                                {String(project.settings.config[key]).startsWith('http') ? (
                                    <a
                                        href={project.settings.config[key]}
                                        target="_blank"
                                        rel="noreferrer"
                                    >
                                        link
                                    </a>
                                ) : (
                                    <span>{String(project.settings.config[key])}</span>
                                )}
                            </li>
                        ))}
                    </ul>
                    <ul className="list-group col-xs-12 col-md-6">
                        <li className="list-group-item list-group-item-info">Model Settings</li>
                        <li className="list-group-item rs-properties__item">
                            <span>Reference X</span>
                            <span>
                                {
                                    project.model_settings.config.wrapper_parameters
                                        .reference_point[0]
                                }
                            </span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>Reference Y</span>
                            <span>
                                {
                                    project.model_settings.config.wrapper_parameters
                                        .reference_point[1]
                                }
                            </span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>Reference Z</span>
                            <span>
                                {
                                    project.model_settings.config.wrapper_parameters
                                        .reference_point[2]
                                }
                            </span>
                        </li>
                        <li className="list-group-item rs-properties__item">
                            <span>Well Section Publicid</span>
                            <span>
                                {
                                    project.model_settings.config.wrapper_parameters
                                        .well_section_publicid
                                }
                            </span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    );
}

function Projects({ selectedProject, setProject }) {
    const [projects, setProjects] = React.useState(null);
    const [status, setStatus] = React.useState('Loading Projects...');

    React.useEffect(() => {
        getProjects()
            .then((json) => json.sort((a, b) => moment(a.starttime) - moment(b.starttime)))
            .then((json) => {
                if (!selectedProject) setProject(json[json.length - 1]);
                return json;
            })
            .then((json) => setProjects(json))
            .catch((e) => {
                console.error(`Error ${e} fetching projects.`);
                setStatus('Error fetching projects.');
                setProjects(null);
            });
    }, []);

    return (
        <ProjectsBox collapseID="#projectList">
            <ul id="projectList" className="collapse list-group" style={{ marginTop: '10px' }}>
                {projects
                    ? projects.map((project) => (
                          <li
                              className={`list-group-item rs-list__item ${
                                  project.id === selectedProject?.id
                                      ? 'list-group-item-warning'
                                      : ''
                              }`}
                              key={`project_${project.id}`}
                          >
                              <div className="container-fluid">
                                  <div className="row">
                                      <div className="col-xs-6 col-md-5">{project.name}</div>
                                      <div className="col-xs-6 col-md-3">{project.starttime}</div>
                                      <div className="col-xs-6 col-md-3">{project.endtime}</div>
                                      <div className="col-xs-6 col-md-1">
                                          <button
                                              className="btn btn-xs btn-primary"
                                              type="button"
                                              onClick={() => setProject(project)}
                                              data-toggle="collapse"
                                              href="#projectList"
                                          >
                                              select
                                          </button>
                                      </div>
                                  </div>
                              </div>
                          </li>
                      ))
                    : ''}
            </ul>
            {projects ? '' : <div>{status}</div>}
            {selectedProject ? <CurrentProject project={selectedProject} /> : ''}
        </ProjectsBox>
    );
}
export default Projects;
