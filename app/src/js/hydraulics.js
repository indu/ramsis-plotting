import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/hydraulics/App';

if (document.getElementById('react-live'))
    ReactDOM.render(<App />, document.getElementById('react-live'));
