import React, { Fragment } from 'react';
import { getResults } from '../../utils/api';
import ModelRunPlots from '../../plots/ModelRunPlots';

function PrintSubkeys({ object }) {
    return Object.keys(object).map((key) => (
        <Fragment key={`item_${key}`}>
            {typeof object[key] === 'object' ? (
                <>
                    {Array.isArray(object[key]) ? (
                        <>
                            <li className="list-group-item rs-properties__item">
                                <span>{key}</span>
                                <span>
                                    {object[key].map((item) => (
                                        <span key={`item_${item}`}>
                                            {String(item)}
                                            <br />
                                        </span>
                                    ))}
                                </span>
                            </li>
                        </>
                    ) : (
                        <>
                            <li className="list-group-item list-group-item-warning">{key}</li>
                            <PrintSubkeys object={object[key]} />
                        </>
                    )}
                </>
            ) : (
                <li className="list-group-item rs-properties__item">
                    <span>{key}</span>
                    <span>{String(object[key])}</span>
                </li>
            )}
        </Fragment>
    ));
}

function SeismicityModelRunProperties({ run, scenario }) {
    return (
        <div className="row rs-properties">
            <ul className="list-group col-xs-12 col-md-6 col-lg-4 rs-properties__list">
                <li className="list-group-item list-group-item-info">Model Info</li>
                <li className="list-group-item rs-properties__item">
                    <span>Model Name</span>
                    <span>{run.model.name}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Model URL</span>
                    <span>{run.model.url}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>SFM WID</span>
                    <span>{run.model.sfmwid}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Seismicity Model Template</span>
                    <span>
                        <small>{run.model.seismicitymodeltemplate}</small>
                    </span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Injection Plan</span>
                    <a
                        href={`${process.env.API_URL}/v1/scenario/${scenario.id}
                    /injectionplan`}
                        target="_blank"
                        rel="noreferrer"
                    >
                        link
                    </a>
                </li>
            </ul>
            <ul className="list-group col-xs-12 col-md-6 col-lg-4 rs-properties__list">
                <li className="list-group-item list-group-item-info">Model Config</li>
                <li className="list-group-item rs-properties__item">
                    <span>Epoch Duration</span>
                    <span>{scenario.seismicitystage.config.epoch_duration}</span>
                </li>
                <PrintSubkeys object={run.config} />
            </ul>
            <ul className="list-group col-xs-12 col-md-6 col-lg-4 rs-properties__list">
                <li className="list-group-item list-group-item-info">Model Bounds</li>
                <li className="list-group-item rs-properties__item">
                    <span>X Min.</span>
                    <span>{run.result.x_min}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>X Max.</span>
                    <span>{run.result.x_max}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Y Min.</span>
                    <span>{run.result.y_min}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Y May</span>
                    <span>{run.result.y_max}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Z Min.</span>
                    <span>{run.result.z_min}</span>
                </li>
                <li className="list-group-item rs-properties__item">
                    <span>Z Max.</span>
                    <span>{run.result.z_max}</span>
                </li>
            </ul>
        </div>
    );
}

function SeismicityModelRun({ runId, scenario, seismicity, hydraulics, injectionplan }) {
    const [run, setRun] = React.useState(null);
    const [status, setStatus] = React.useState('loading...');

    React.useEffect(() => {
        getResults(runId).then((results) => {
            if (results.status === 'COMPLETE' && 'result' in results) {
                setRun(results);
            } else setStatus('No Data Available.');
        });
    }, [runId]);

    return (
        <div className="panel-body">
            {run ? (
                <>
                    <div className="page-header">
                        <h3 className="h2">
                            Run {run.id} <small>Status: {run.status}</small>
                        </h3>
                        <button
                            className="btn btn-primary"
                            type="button"
                            data-toggle="collapse"
                            data-target={`#collapseDetails${run.id}`}
                            aria-expanded="false"
                            aria-controls={`collapseDetails${run.id}`}
                        >
                            Show Details &nbsp;&nbsp;
                            <span className="glyphicon glyphicon-chevron-down" aria-hidden="true" />
                        </button>
                    </div>
                    <div className="container-fluid">
                        <div id={`collapseDetails${run.id}`} className="collapse">
                            <SeismicityModelRunProperties run={run} scenario={scenario} />
                        </div>
                        <ModelRunPlots
                            results={run}
                            hydraulics={
                                hydraulics.sections.find(
                                    (s) =>
                                        s.publicid ===
                                        run.config.wrapper_parameters.well_section_publicid
                                ).hydraulics
                            }
                            injectionplan={
                                injectionplan.sections.find(
                                    (s) =>
                                        s.publicid ===
                                        run.config.wrapper_parameters.well_section_publicid
                                ).injectionplan
                            }
                            seismicity={seismicity}
                        />
                    </div>
                </>
            ) : (
                <p>{status}</p>
            )}
        </div>
    );
}

export default SeismicityModelRun;
