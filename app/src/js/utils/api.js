import { xmlToJson } from './helpers';

export function getWell(forecastID) {
    let hyd = fetch(`${process.env.API_URL}/v1/forecast/${forecastID}/well`).then((response) =>
        response.json()
    );
    return hyd;
}

export function getResults(id) {
    let r = fetch(`${process.env.API_URL}/v1/seismicitymodelrun/${id}`).then((response) =>
        response.json()
    );
    return r;
}

export function getForecasts(status = [], start = '', end = '') {
    let statusString = '';
    status.forEach((item) => (statusString += `&status=${item}`));
    let startString = start ? `starttime=${start}` : '';
    let endString = end ? `&endtime=${end}` : '';
    let f = fetch(
        `${process.env.API_URL}/v1/forecast/?${startString}${endString}${statusString}`
    ).then((response) => response.json());
    return f;
}

export function getForecastsByProject(projectID) {
    let f = fetch(`${process.env.API_URL}/v1/project/${projectID}/forecast`).then((response) =>
        response.json()
    );
    return f;
}

export function getScenario(id) {
    let s = fetch(`${process.env.API_URL}/v1/scenario/${id}`).then((response) => response.json());
    return s;
}

export function getForecastSeismicity(id) {
    let s = fetch(`${process.env.API_URL}/v1/forecast/${id}/catalog`).then((response) =>
        response.json()
    );
    return s;
}

export function getScenarioInjectionPlan(id) {
    let i = fetch(`${process.env.API_URL}/v1/scenario/${id}/injectionplan`).then((response) =>
        response.json()
    );
    return i;
}

export function getProjects(starttime = null) {
    let p = fetch(
        `${process.env.API_URL}/v1/project/${starttime ? `?starttime=${starttime}` : ''}`
    ).then((response) => response.json());
    return p;
}

export function getBoreholesData() {
    // let p = fetch(`${process.env.HYDWS_URL}/hydws/v1/boreholes`,
    // { credentials: 'include' }).then(
    let p = fetch(`${process.env.HYDWS_URL}/hydws/v1/boreholes`).then((response) =>
        response.json()
    );
    return p;
}

export function getBoreholeHydraulics(bhId, starttime = null, endtime = null) {
    let p = fetch(
        `${process.env.HYDWS_URL}/hydws/v1/boreholes/${bhId}?level=hydraulic${
            starttime ? `&starttime=${starttime}` : ''
        }${endtime ? `&endtime=${endtime}` : ''}`
        // { credentials: 'include' }
    ).then((response) => response.json());
    return p;
}

export function getSectionHydraulics(bhId, scId, starttime = null, endtime = null) {
    let p = fetch(
        `${process.env.HYDWS_URL}/hydws/v1/boreholes/${bhId}/sections/${scId}/hydraulics?${
            starttime ? `&starttime=${starttime}` : ''
        }${endtime ? `&endtime=${endtime}` : ''}`
    ).then((response) => response.json());
    return p;
}

export function getSeismicity(startTime, endTime) {
    let p = fetch(
        `${process.env.FDSNWS_URL}/fdsnws/event/1/query?starttime=${startTime}&endtime=${endTime}`
        // {
        // credentials: 'include',
        // headers: { 'Content-Type': 'text/xml' },
        // }
    )
        .then((response) => response.text())
        .then((data) => {
            if (data) {
                let parser = new DOMParser();
                let xmlDoc = parser.parseFromString(data, 'text/xml');
                return xmlToJson(xmlDoc.getElementsByTagName('eventParameters')[0]);
            }
            return {};
        });
    return p;
}
