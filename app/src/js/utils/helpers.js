import moment from 'moment';

export function b64EncodeUnicode(str) {
    return btoa(str);
}

export function b64DecodeUnicode(str) {
    return atob(str);
}

export function median(arr) {
    const mid = Math.floor(arr.length / 2);
    const nums = [...arr].sort((a, b) => a - b);
    return arr.length % 2 !== 0 ? nums[mid] : (nums[mid - 1] + nums[mid]) / 2;
}
export function number(x) {
    return x === null ? NaN : +x;
}

export function quantile(arr, q) {
    const sorted = [...arr].sort((a, b) => a - b);
    const pos = (sorted.length - 1) * q;
    const base = Math.floor(pos);
    const rest = pos - base;

    if (sorted[base + 1] !== undefined) {
        return sorted[base] + rest * (sorted[base + 1] - sorted[base]);
    }

    return sorted[base];
}

export function histogram(X, binRange) {
    // inclusive of the first number
    let max = Math.max(...X);
    let min = Math.min(...X);
    let len = max - min + 1;
    let numberOfBins = Math.ceil(len / binRange);
    let bins = new Array(numberOfBins).fill(0);
    // -min to normalise values for the array
    X.forEach((x) => bins[Math.floor((x - min) / binRange)]++);
    return bins;
}

export function makeArr(startValue, stopValue, cardinality) {
    let arr = [];
    let step = (stopValue - startValue) / (cardinality - 1);
    for (let i = 0; i < cardinality; i++) {
        arr.push(startValue + step * i);
    }
    return arr;
}

export function round(num, prec) {
    let m = Number((Math.abs(num) * 10 ** prec).toPrecision(15));
    return (Math.round(m) / 10 ** prec) * Math.sign(num);
}

export function xmlToJson(xml) {
    // Create the return object
    let obj = {};

    if (xml.nodeType === 1) {
        // do attributes
        if (xml.attributes.length > 0) {
            for (let j = 0; j < xml.attributes.length; j++) {
                let attribute = xml.attributes.item(j);
                obj[attribute.nodeName] = attribute.nodeValue;
            }
        }
    }

    // do children
    if (xml.hasChildNodes()) {
        for (let i = 0; i < xml.childNodes.length; i++) {
            let item = xml.childNodes.item(i);

            if (item.nodeType === 3)
                return (
                    moment(
                        item.nodeValue.substring(0, 19),
                        'YYYY-MM-DD[T]HH:mm:ss',
                        true
                    ).valueOf() || // datetime
                    parseFloat(item.nodeValue) || // float
                    item.nodeValue // string
                );

            let { nodeName } = item;
            if (typeof obj[nodeName] === 'undefined') {
                obj[nodeName] = xmlToJson(item);
            } else {
                if (typeof obj[nodeName].push === 'undefined') {
                    let old = obj[nodeName];
                    obj[nodeName] = [];
                    obj[nodeName].push(old);
                }
                obj[nodeName].push(xmlToJson(item));
            }
        }
    }
    return obj;
}

export function sortDatetimeValue(arr) {
    return arr.sort((a, b) => moment(a.datetime.value) - moment(b.datetime.value));
}
