import React from 'react';
import { getScenario, getScenarioInjectionPlan } from '../../utils/api';

import SeismicityModelRun from './SeismicityModelRun';

function SeismicityStage({ hydraulics, seismicity, scenarioId }) {
    const [injectionplan, setInjectionPlan] = React.useState(null);
    const [scenario, setScenario] = React.useState(null);
    const [status, setStatus] = React.useState('loading...');

    React.useEffect(() => {
        getScenarioInjectionPlan(scenarioId)
            .then((json) => setInjectionPlan(json))
            .catch((e) => {
                setInjectionPlan(null);
                setStatus('Error loading injectionplan.');
                console.error(`Error loading injectionplan: ${e}`);
            });
        getScenario(scenarioId)
            .then((json) => setScenario(json))
            .catch((e) => {
                setScenario(null);
                setStatus('Error loading scenario.');
                console.error(`Error loading scenario: ${e}`);
            });
    }, [scenarioId]);

    React.useEffect(() => {
        if (hydraulics === 'error') setStatus('Error loading hydraulics.');
        else if (seismicity === 'error') setStatus('Error loading seismicity.');
        else if (hydraulics && seismicity && scenario && injectionplan) setStatus('ok');
    }, [scenario, injectionplan, hydraulics, seismicity]);

    return (
        <div className="panel panel-info rs-box rs-box--scenario">
            <div className="panel-heading">
                <div className="rs-box__heading">
                    <h2 className="h2">Seismicity {scenario ? scenario.name : 'Scenario'}</h2>
                </div>
            </div>

            <div className="panel-body">
                {status === 'ok' ? (
                    scenario.seismicitystage.runs.map((id) => (
                        <SeismicityModelRun
                            key={`modelrun_${id}`}
                            runId={id}
                            scenario={scenario}
                            seismicity={seismicity}
                            hydraulics={hydraulics}
                            injectionplan={injectionplan}
                        />
                    ))
                ) : (
                    <p>{status}</p>
                )}
            </div>
        </div>
    );
}
export default SeismicityStage;
