import React from 'react';
import moment from 'moment';

import DateRangePicker from '../../utils/DateRangePicker';
import { getSectionHydraulics } from '../../utils/api';
import { sortDatetimeValue } from '../../utils/helpers';
import HydraulicsPlot from '../../plots/HydraulicsPlot';
import useInterval from '../../utils/useInterval';

// Config to control the plots which are displayed
const DISPLAY = {
    plot1: {
        title: 'Injection Borehole ST1',
        subtitle: 'Interval 11',
        data: {
            '29a3a2d8-b5d3-4dd6-bc12-0892874723fc': {
                '88b16864-7688-40a1-91cd-d18f5d64649a': [
                    { toppressure: 'Pressure' },
                    { bottompressure: 'Uphole Pressure Sensor' },
                    { topflow: 'Injection Rate' },
                ],
            },
        },
    },
    plot2: {
        title: 'Pressure ST1',
        subtitle: '',
        data: {
            '29a3a2d8-b5d3-4dd6-bc12-0892874723fc': {
                'c0c71ae8-e37a-4ad1-9e91-0407cf0792b1': [{ toppressure: 'Section 2' }],
                'f4bfa3dd-d363-4d92-b294-80b69c165ba4': [{ toppressure: 'Section 3' }],
                '1faab1ce-726b-4554-9500-3648de704c8a': [{ toppressure: 'Section 5' }],
                '419474b3-65b2-41d6-aa10-21be0b6208b0': [{ toppressure: 'Section 6' }],
                'b1fbb58f-38eb-4b0e-a073-bd03195c692d': [{ toppressure: 'Section 8' }],
                'fd5f3929-3a01-451a-976b-7745390ebb93': [{ toppressure: 'Section 9' }],
                '88b16864-7688-40a1-91cd-d18f5d64649a': [{ toppressure: 'Section 11' }],
                '350706fb-231d-41e8-b9fd-5aa006446df6': [{ toppressure: 'Section 12' }],
                '40a79ca8-875f-4f9e-9e87-0f11e3c1727a': [{ toppressure: 'Section 14' }],
            },
        },
    },
    plot3: {
        title: 'Pressure MB2',
        subtitle: '',
        data: {
            '0e933794-a0a1-4bca-a5b7-6c8e22f5d032': {
                '006ae5e2-cdf6-4989-aa11-34c51e823066': [{ toppressure: 'Section 1' }],
                '9fba4d4d-7817-49b6-9296-64e6a0304908': [{ toppressure: 'Section 2' }],
                '04257e22-9cbd-4d9c-9af5-92bbfd440ff9': [{ toppressure: 'Section 3' }],
                '0eba7fe8-ff0e-4c74-8660-672fc14e7a90': [{ toppressure: 'Section 4' }],
                '85c882a9-3245-4b65-b3f5-bfcacb9b8f47': [{ toppressure: 'Section 5' }],
                '0ebe7fbd-7217-4458-86b9-84e5fb4eca4a': [{ toppressure: 'Section 6' }],
                '7494e2b5-8620-4e5e-b304-d501f27497a3': [{ toppressure: 'Section 7' }],
            },
        },
    },
};

// dict of boreholes and sets of respective sections which need to be fetched
// {"bhID1": ['secID11', 'secID12'],
//  "bhID2": ['secID21', 'secID22']}
const BOREHOLES = Object.values(DISPLAY).reduce((acc, value) => {
    Object.entries(value.data).forEach(([k, v]) => {
        let kb64 = k;
        if (!(kb64 in acc)) acc[kb64] = new Set();
        Object.keys(v).forEach((l) => {
            acc[kb64].add(l);
        });
    });
    return acc;
}, {});

function App() {
    const [startDate, setStartDate] = React.useState(moment(6, 'HH').toDate());
    const [endDate, setEndDate] = React.useState(moment().endOf('day').toDate());

    const [status, setStatus] = React.useState(null);
    const [hydraulics, setHydraulics] = React.useState(null);

    let updateData = () => {
        let start = moment(startDate).format('YYYY-MM-DDTHH:mm:ss');
        let end = moment(endDate).format('YYYY-MM-DDTHH:mm:ss');

        Promise.all(
            // returns a 1D array of Promises
            Object.entries(BOREHOLES)
                .map(([boreholeId, sections]) =>
                    [...sections].map((sectionId) =>
                        getSectionHydraulics(boreholeId, sectionId, start, end)
                    )
                )
                .flat()
        )
            .then((values) => {
                setHydraulics(
                    // returns a key-value object with "sectionid: hydraulics"
                    Object.fromEntries(
                        Object.values(BOREHOLES)
                            .map((sections) =>
                                [...sections].map((sectionId) => {
                                    let hyd = values.shift();
                                    if (!Array.isArray(hyd)) hyd = [];
                                    return [sectionId, sortDatetimeValue(hyd)];
                                })
                            )
                            .flat(1)
                    )
                );
                setStatus('ok');
            })
            .catch((error) => {
                console.log(error);
                setStatus('Error Encountered.');
            });
    };

    let requestUpdate = () => {
        setStatus('Loading Data...');
        updateData();
    };

    useInterval(updateData, 45000);

    React.useEffect(() => {
        setStatus('Loading Data...');
        updateData();
    }, []);

    return (
        <div className="container">
            <div className="page-header">
                <h1 className="h1">Live Data</h1>
            </div>
            <DateRangePicker
                start={startDate}
                setStart={setStartDate}
                end={endDate}
                setEnd={setEndDate}
                onSubmit={requestUpdate}
                disabled={status === 'Loading Data...'}
            />

            <div className="container" style={{ marginTop: '20px' }}>
                {hydraulics && status === 'ok'
                    ? Object.values(DISPLAY).map((plot) => (
                          <div key={plot.title}>
                              <div className="page-header">
                                  <h2 className="h2">
                                      {plot.title} <small>{plot.subtitle}</small>
                                  </h2>
                              </div>
                              <HydraulicsPlot key={plot.title} data={hydraulics} plot={plot} />
                          </div>
                      ))
                    : status}
            </div>
        </div>
    );
}

export default App;
