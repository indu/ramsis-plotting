import moment from 'moment';
import React from 'react';
import Plot from 'react-plotly.js';
import useInterval from '../utils/useInterval';

const FIELDS = {
    toppressure: { conv: (d) => d, axis: 'y2' },
    bottompressure: { conv: (d) => d, axis: 'y2' },
    topflow: { conv: (d) => d * 60000, axis: 'y1' },
    bottomflow: { conv: (d) => d * 60000, axis: 'y1' },
};

export default function HydraulicsPlot({ data, plot }) {
    let plotData = [];

    // display data comes grouped by borehole
    Object.values(plot.data).forEach((borehole) => {
        // for each section, multiple values can be displayed (like temp, pressure etc.)
        Object.entries(borehole).forEach(([section, lines]) => {
            lines.forEach((line) => {
                let [realValue, legend] = Object.entries(line)[0];

                // "data" is a dict of sectionid's
                let x = data[section].map((e) => moment(e.datetime.value).valueOf());
                let y = data[section].map((e) => FIELDS[realValue].conv(e[realValue].value));

                let pl = {
                    x: x.length > 0 ? x : [null], // use [null] so that legend gets displayed
                    y: y.length > 0 ? y : [null],
                    type: 'scatter',
                    mode: 'lines',
                    name: legend,
                    yaxis: FIELDS[realValue].axis,
                };

                plotData.push(pl);
            });
        });
    });

    return (
        <Plot
            data={plotData}
            layout={{
                showlegend: true,
                legend: { orientation: 'h', xanchor: 'center', y: -0.15, x: 0.5 },
                autosize: true,
                xaxis: {
                    type: 'date',
                    title: 'Time [UTC]',
                },
                yaxis: {
                    title: 'Flowrate [l/min]',
                },
                yaxis2: {
                    title: 'Pressure [Pa]',
                    overlaying: 'y',
                    side: 'right',
                },
            }}
            config={{ responsive: true }}
        />
    );
}
