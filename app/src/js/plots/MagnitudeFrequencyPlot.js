import React from 'react';
import Plot from 'react-plotly.js';
import { median, quantile, makeArr, round } from '../utils/helpers';

export default function MagnitudeFreuencyPlot({ results, seismicity }) {
    // Result Samples
    let { samples } = results.result.subgeometries[0];

    let modelParameters = results.config.model_parameters;

    // Fitted Data
    let fitSamples = samples.filter((sample) => sample.numberevents.value);
    let y0 = fitSamples.map((s) => s.numberevents.value);

    // Predicted data
    let predSamples = samples.filter((sample) => sample.numberevents.pdfvariable);

    let y = predSamples.map((s) => median(s.numberevents.pdfvariable));
    let mc = predSamples.map((s) => s.mc.value);
    let bValue = predSamples.map((s) => median(s.b.pdfvariable));
    let bLower = predSamples.map((s) => quantile(s.b.pdfvariable, 0.25));
    let bUpper = predSamples.map((s) => quantile(s.b.pdfvariable, 0.75));
    let yLower = predSamples.map((s) => quantile(s.numberevents.pdfvariable, 0.25));
    let yUpper = predSamples.map((s) => quantile(s.numberevents.pdfvariable, 0.75));

    // This should actually be loaded from the database
    let alpha = predSamples.map((s) => median(s.alpha?.pdfvariable || [0]));

    // observed seismicity
    let mag = seismicity.events.map((ev) => ev.magnitude.value);

    let labelModel = `Model Fit (Mc: ${mc.slice(-1).toString()}, b: ${round(
        bValue.slice(-1),
        3
    ).toString()}, af: ${round(alpha.slice(-1), 3).toString()})`;

    // PLOT 2: frequency magnitude distribution

    let aValueFit = Math.log10(y0.reduce((partialSum, a) => partialSum + a, 0)) + mc[0] * bValue[0];

    let Nev = y.reduce((partialSum, a) => partialSum + a, 0) + y0.reduce((partialSum, a) => partialSum + a, 0)
    let NevUP = yUpper.reduce((partialSum, a) => partialSum + a, 0) + y0.reduce((partialSum, a) => partialSum + a, 0)
    let NevLO = yLower.reduce((partialSum, a) => partialSum + a, 0) + y0.reduce((partialSum, a) => partialSum + a, 0)
    let aValuePred = Math.log10(Nev) + mc.slice(-1) * bValue.slice(-1);
    let aUpper = Math.log10(NevUP) + mc.slice(-1) * bValue.slice(-1);
    let aLower = Math.log10(NevLO) + mc.slice(-1) * bValue.slice(-1);

    let maxmag = aValuePred/bValue.slice(-1)

    let binRange = modelParameters.dM; // this is a property of the Model
    //let numberOfBins = Math.ceil((Math.max(...mag) - Math.min(...mag)) / binRange);
    //let magXBins = makeArr(Math.min(...mag) - binRange, Math.max(...mag) + binRange, numberOfBins);
    let numberOfBins = Math.ceil((maxmag - Math.min(...mag)) / binRange);
    let magXBins = makeArr(Math.min(...mag) - binRange, maxmag + binRange, numberOfBins);

    let grFit = magXBins.map((x) => 10 ** (aValueFit - x * bValue[0]));

    let grPred = magXBins.map((x) => 10 ** (aValuePred - x * bValue.slice(-1)));
    let grPredUpper = magXBins.map((x) => 10 ** (aUpper - x * bUpper.slice(-1)));
    let grPredLower = magXBins.map((x) => 10 ** (aLower - x * bLower.slice(-1)));
    let grPredBound = grPredUpper.concat(grPredLower.slice().reverse());

    let histCum = {
        x: mag,
        type: 'histogram',
        cumulative: { enabled: true, direction: 'decreasing' },
        xbins: {
            //end: Math.max(mag),
            end: maxmag,
            size: binRange, // this is a property of the model
            start: Math.min(mag),
        },
        marker: {
            color: 'rgba(255, 100, 102, 0.7)',
            line: {
                color: 'rgba(255, 100, 102, 1)',
                width: 1,
            },
        },
        opacity: 0.5,
        name: 'Data cumul.',
    };

    let histNonCum = {
        x: mag,
        type: 'histogram',
        xbins: {
            //end: Math.max(mag),
            end: maxmag,
            size: binRange, // this is a property of the model
            start: Math.min(mag),
        },
        marker: {
            color: 'rgba(100, 200, 102, 0.7)',
            line: {
                color: 'rgba(100, 200, 102, 1)',
                width: 1,
            },
        },
        opacity: 0.5,
        name: 'Data non cum.',
    };

    let fitLine = {
        x: magXBins,
        y: grFit,
        mode: 'lines',
        type: 'scatter',
        name: labelModel,
        line: { width: 3, color: 'red' },
    };

    let predLine = {
        x: magXBins,
        y: grPred,
        mode: 'lines',
        type: 'scatter',
        name: 'Model Prediction',
        line: { width: 3, color: 'rgba(0,100,80,0.2)' },
    };

    let predBound = {
        x: magXBins.concat(magXBins.slice().reverse()),
        y: grPredBound,
        fill: 'toself',
        fillcolor: 'rgba(0,100,80,0.2)',
        line: { color: 'transparent' },
        name: 'Model Prediction Range',
        type: 'scatter',
    };

    let dataplot = [histCum, histNonCum, fitLine, predLine, predBound];
    let layout = {
        title: 'Frequency magnitude distribution',
        autosize: true,
        responsive: true,
        xaxis: {
            title: 'Magnitude',
        },
        yaxis: {
            title: 'Number of Events',
            type: 'log',
            autorange: true,
        },
        barmode: 'overlay',
    };

    return <Plot data={dataplot} layout={layout} config={{ responsive: true }} />;
}
